#!/usr/bin/bash
folders=(images scripts fonts audio)

for f in "${folders[@]}"; do
	pushd "mod/$f" &>/dev/null
	echo "Packing $f"
	files=()
	for file in *; do
		if ! [[ "$file" == *.rpy ]]; then
			files+=("$file")
		fi
	done
	rpatool -c "../../$f.rpa" "${files[@]}"
	popd &>/dev/null
done
