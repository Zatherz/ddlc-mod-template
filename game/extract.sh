#!/usr/bin/bash

if [ -z "$DDLC_ORIG" ]; then
	DDLC_ORIG="$PWD/.orig"
fi

function unpack {
	local name="$1"

	mkdir -p "$1"
	pushd "$1" &>/dev/null
	rpatool -x "$DDLC_ORIG/$1.rpa"
	popd &>/dev/null
}

pushd "$DDLC_ORIG" &>/dev/null
for f in *.rpa; do
	popd &>/dev/null
	dirname="$(sed 's/\.rpa$//' <<< "$f")"
	echo "Unpacking: $dirname"
	if [ -e "$dirname" ]; then
		echo "  cleaning up from last extraction..."
		rm -rf "$dirname"
	fi
	unpack "$dirname"
	if [ -e "$f" ]; then
		echo "  removing .rpa"
		rm "$f"
	fi
	pushd "$DDLC_ORIG" &>/dev/null
done
popd &>/dev/null
