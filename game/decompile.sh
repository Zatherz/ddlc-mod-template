#!/usr/bin/bash

if ! [ -d "scripts" ]; then
	echo "You have to extract scripts first! Try ./extract.sh" 2>&1
	exit 1
fi

find scripts -iname "*.rpyc" -exec bash -c "
	echo Decompiling {}
	unrpyc -c {} &>/dev/null
	targetname=\"\$(sed 's/\\.rpyc\$/.rpy/' <<< "{}")\"
	if ! [ -e \"\$targetname\" ]; then
	  echo \"  FAILED decompiling {}!\"
	  exit 1
	fi
	echo \"  removing .rpyc\"
	rm "{}"
" \;
