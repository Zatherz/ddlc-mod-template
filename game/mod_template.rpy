# don't remove this file as it is necessary for the
# template to run in its current state
# 
# feel free to get rid of the prints if you need to,
# though.

init python:
    print("Zatherz's Mod Template Entry Point (mod loaded!)")

    # we have to fix the search path so that the game
    # can run extracted (without the use of .rpas)
    # but so that we can still keep 'scripts', 'images' etc
    # separate

    config.searchpath.append(config.searchpath[0] + "/scripts")
    config.searchpath.append(config.searchpath[0] + "/images")
    config.searchpath.append(config.searchpath[0] + "/audio")
    config.searchpath.append(config.searchpath[0] + "/fonts")

    print("New search path: ")
    print(config.searchpath)

